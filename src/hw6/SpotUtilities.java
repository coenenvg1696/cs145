package hw6;

import java.io.File;

import hw6.speccheck.GifSequenceWriter;

public class SpotUtilities {
	public static int[] countNeighbors(boolean[][] bitmap, int ho, int vo, int hi, int vi, int column, int row) {
		int[] array = new int[2];

		for (int r = row - vo; r <= row + vo; r++) {

			for (int c = column - ho; c <= column + ho; c++) {

				int rowX = Math.abs(row - r);
				int columnX = Math.abs(column - c);
				double outsideDis = Math.pow(columnX, 2) / Math.pow(ho, 2) + Math.pow(rowX, 2) / Math.pow(vo, 2);
				double insideDis = Math.pow(columnX, 2) / Math.pow(hi, 2) + Math.pow(rowX, 2) / Math.pow(vi, 2);

				if (BitmapUtilities.isOn(bitmap, c, r) && outsideDis <= 1) {

					if (insideDis <= 1) {
						array[0]++;

					} else {

						array[1]++;
					}
				}
			}
		}

		return array;
	}

	public static boolean[][] step(boolean[][] bitmap, int ho, int vo, int hi, int vi, double ratio) {
		boolean[][] newBitmap = BitmapUtilities.create(bitmap[0].length, bitmap.length);

		for (int r = 0; r < bitmap.length; r++) {

			for (int c = 0; c < bitmap[r].length; c++) {

				int[] neighbors = countNeighbors(bitmap, ho, vo, hi, vi, c, r);
				double distanceApart = neighbors[0] - ratio * neighbors[1];

				if (Math.abs(distanceApart) < 0.001) {

					newBitmap[r][c] = bitmap[r][c];

				} else if (distanceApart > 0) {

					newBitmap[r][c] = true;

				} else {

					newBitmap[r][c] = false;
				}
			}
		}
		return newBitmap;
	}

	public static int converge(boolean[][] bitmap, int ho, int vo, int hi, int vi, double ratio, File animation,
			int maxSteps) {

		boolean[][] before = BitmapUtilities.clone(bitmap);
		boolean[][] after = step(before, ho, vo, hi, vi, ratio);

		GifSequenceWriter output = new GifSequenceWriter(animation, 200, true);
		output.appendFrame(BitmapUtilities.toBufferedImage(bitmap));
		int i = 1;

		while (i < maxSteps && (!BitmapUtilities.equals(before, after))) {
			output.appendFrame(BitmapUtilities.toBufferedImage(after));
			before = after;
			after = step(before, ho, vo, hi, vi, ratio);
			i++;
		}

		output.close();
		return i + 1;
	}
}