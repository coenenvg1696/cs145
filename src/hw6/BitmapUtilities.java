package hw6;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;

public class BitmapUtilities {

	public static boolean[][] create(int width, int height) {

		boolean[][] bitmap = new boolean[height][width];
		return bitmap;
	}

	public static void randomize(boolean[][] bitmap, long stem) {
		Random generator = new Random(stem);

		for (int c = 0; c < bitmap.length; c++) {
			for (int r = 0; r < bitmap[c].length; r++) {
				bitmap[c][r] = generator.nextBoolean();
			}
		}
	}

	public static void write(boolean[][] bitmap, File file) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(file);
		writer.println("P1");
		writer.println(bitmap[0].length + " " + bitmap.length);

		for (int c = 0; c < bitmap.length; c++) {

			for (int r = 0; r < bitmap[c].length; r++) {

				if (bitmap[c][r]) {

					writer.println(0);
				} else {

					writer.println(1);
				}
			}
		}
		writer.close();
	}

	public static BufferedImage toBufferedImage(boolean[][] bitmap) {
		BufferedImage image = new BufferedImage(bitmap[0].length, bitmap.length, BufferedImage.TYPE_BYTE_BINARY);

		for (int c = 0; c < bitmap.length; c++) {

			for (int r = 0; r < bitmap[c].length; r++) {

				if (bitmap[c][r]) {

					image.setRGB(r, c, Color.BLACK.getRGB());
				} else {

					image.setRGB(r, c, Color.WHITE.getRGB());
				}
			}
		}
		return image;
	}

	public static boolean equals(boolean[][] first, boolean[][] second) {
		for (int c = 0; c < first.length; c++) {

			for (int r = 0; r < first[c].length; r++) {

				if (first[c][r] != second[c][r]) {
					return false;
				}
			}
		}
		return true;
	}

	public static boolean[][] clone(boolean[][] bitmap) {
		boolean[][] bitmapNew = create(bitmap[0].length, bitmap.length);
		for (int c = 0; c < bitmap.length; c++) {

			for (int r = 0; r < bitmap[c].length; r++) {

				bitmapNew[c][r] = bitmap[c][r];
			}
		}
		return bitmapNew;
	}

	public static int wrapIndex(int topBound, int index) {
		if (index >= 0) {

			return index % topBound;
		} else {

			return (topBound + index % topBound) % topBound;
		}

	}

	public static boolean isOn(boolean[][] bitmap, int column, int row) {

		return bitmap[wrapIndex(bitmap.length, row)][wrapIndex(bitmap[0].length, column)];
	}
}