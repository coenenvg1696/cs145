package hw4;

import java.io.File;

public class FilenameUtilities {
	
	public static String getExtension(File fileName) {
		
		String sringFile = fileName.getName();
		
		if (sringFile.indexOf(".") != -1 && sringFile.lastIndexOf(".") != 0) {
			
			String fileEnd = sringFile.substring(sringFile.lastIndexOf('.') + 1, sringFile.length());
			return fileEnd;
			
		} else {
			return null;
		}
	}

	public static File appendToName(File file, String addition) {
		
		String fileName = file.toString();
		String extension = getExtension(file);
		
		if (extension == null) {
			
			fileName += "_" + addition;
			
		} else {
			
			fileName = fileName.substring(0, fileName.lastIndexOf(".")) + "_" + addition + "." + extension;
		}
		return new File(fileName);
	}
}
