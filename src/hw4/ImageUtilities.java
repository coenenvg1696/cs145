package hw4;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class ImageUtilities {

	public static BufferedImage swapCorners(BufferedImage image) {
		
		int halfHeight = image.getHeight() / 2;
		int halfWidth = image.getWidth() / 2;
		BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
		
		for (int c = 0; c < image.getWidth(); c++) {
			
			for (int r = 0; r < image.getHeight(); r++) {
				
				newImage.setRGB(c, r, image.getRGB((c + halfWidth) % image.getWidth(), (r + halfHeight) % image.getHeight()));
			}
		}
		return newImage;
	}

	public static BufferedImage getCircleMask(int width, int height, double power) {
		
		BufferedImage masked = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		double middleC = (double) width / 2;
		double middleR = (double) height / 2;

		for (int c = 0; c < width; c++) {
			
			for (int r = 0; r < height; r++) {

				double distFromCenter = Math.sqrt(Math.pow(c - middleC, 2) + Math.pow(r - middleR, 2));
				double radius = Math.min(middleC, middleR);

				if (distFromCenter <= radius) {
					
					float regDistance = (float) (distFromCenter / radius);
					float newDistance = (float) (Math.pow(regDistance, power));
					float gray = 1 - newDistance;

					Color intColor = new Color(gray, gray, gray);
					masked.setRGB(c, r, intColor.getRGB());
					
				} else {
					
					masked.setRGB(c, r, Color.BLACK.getRGB());

				}
			}
		}
		return masked;
	}

	public static int mix(int a, int b, double weight) {
		
		double blend = weight * a + ((1 - weight) * b + .000000001);
		return (int) blend;
	}

	public static Color mix(Color a, Color b, double weight) {
		
		Color mixedColor = new Color(mix(a.getRed(), b.getRed(), weight), mix(a.getGreen(), b.getGreen(), weight), mix(a.getBlue(), b.getBlue(), weight));
		return mixedColor;
	}

	public static BufferedImage addMasked(BufferedImage a, BufferedImage maskA, BufferedImage b, BufferedImage maskB) {
		
		BufferedImage finale = new BufferedImage(a.getWidth(), a.getHeight(), a.getType());
		
		for (int c = 0; c < a.getWidth(); c++) {
			
			for (int r = 0; r < a.getHeight(); r++) {
				
				int weightA = new Color(maskA.getRGB(c, r)).getRed();
				int weightB = new Color(maskB.getRGB(c, r)).getRed();
				int weightSum = weightA + weightB;
				if (weightSum == 0) {finale.setRGB(c, r, mix(new Color(a.getRGB(c, r)), new Color(b.getRGB(c, r)), 0.5).getRGB());

				} else {
					
					double weightProportion = (double) weightA / weightSum; 
					finale.setRGB(c, r, mix(new Color(a.getRGB(c, r)), new Color(b.getRGB(c, r)), weightProportion).getRGB());
				}
			}
		}
		return finale;
	}

	public static BufferedImage makeSeamless(BufferedImage image, double power) {
		
		BufferedImage image2 = swapCorners(image);
		BufferedImage image3 = getCircleMask(image.getWidth(), image.getHeight(), power);
		BufferedImage image4 = swapCorners(image3);
		BufferedImage image5 = addMasked(image, image3, image2, image4);
		return image5;
	}

	public static BufferedImage tile(BufferedImage image, int hor, int vert) {
		
		BufferedImage tiledImage = new BufferedImage(image.getWidth() * hor, image.getHeight() * vert, image.getType());

		for (int c = 0; c < tiledImage.getWidth(); c++) {
			
			for (int r = 0; r < tiledImage.getHeight(); r++) {
				
				tiledImage.setRGB(c, r, image.getRGB(c % image.getWidth(), r % image.getHeight()));
			}
		}
		return tiledImage;
	}
}