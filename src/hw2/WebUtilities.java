package hw2;

public class WebUtilities {
	
	public static void main(String[] args){		
	}
	
	public static String getImageLink(String imageFile, int width, int height) {
		
		String link = "<a href=\"" + imageFile + "\"><img src=\"" + imageFile + "\" width=\"" + width + "\" height=\"" + height + "\"></a>";
		
		return link;	
	}
	
	public static String getHost(String fullURL){
		
		int slashLocation = fullURL.indexOf("/") + 1;
		String host = fullURL.substring(slashLocation + 1,  fullURL.indexOf("/", slashLocation + 1));
		
		return host;
	}
	
	
	public static String getTitle(String fullHTML) {

		int titleLocation = fullHTML.indexOf("<title>") + 6;
		String shortHTML = fullHTML.substring(titleLocation + 1, fullHTML.indexOf("</", titleLocation + 1));
		
		return shortHTML;
	}
	
	public static String invertHexColor(String htmlColor) {

		int redToInt = Integer.parseInt(htmlColor.substring(htmlColor.indexOf("#") + 1, 3), 16);
		int greenToInt = Integer.parseInt(htmlColor.substring(htmlColor.indexOf("#") + 3, 5), 16);
		int blueToInt = Integer.parseInt(htmlColor.substring(htmlColor.indexOf("#") + 5, 7), 16);

		int oppositeRed = 255 - redToInt;
		int oppositeGreen = 255 - greenToInt;
		int oppositeBlue = 255 - blueToInt;

		String backToStringRed = String.format("%02x", oppositeRed);
		String backToStringGreen = String.format("%02x", oppositeGreen);
		String backToStringBlue = String.format("%02x", oppositeBlue);

		return "#" + backToStringRed + backToStringGreen + backToStringBlue;
	}
}
