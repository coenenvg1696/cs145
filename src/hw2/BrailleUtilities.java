package hw2;

public class BrailleUtilities {
	
	public final static String RAISED = "\u2022";
	public final static String UNRAISED = "\u00b7";
	public final static String LETTER_SPACER = "  ";
	public final static String WORD_SPACER = "    ";

	public static void main(String[] args) {
	
	System.out.println(translate("The quick brown fox jumps over the lazy Dog"));
	}
	
	public static String translateTopLine(String text) {

	String lowText = text.trim().toLowerCase().replace(" ", WORD_SPACER);

	String highText = lowText.replaceAll("[abehkloruvz]", RAISED + UNRAISED + LETTER_SPACER)
	.replaceAll("[stwij]", UNRAISED + RAISED + LETTER_SPACER)
	.replaceAll("[cdfgmnpqxy]", RAISED + RAISED + LETTER_SPACER);

	return highText.trim();
	}

	
	public static String translateMiddleLine(String text2) {

	String lowText = text2.trim().toLowerCase().replace(" ", WORD_SPACER);

	String midText = lowText.replaceAll("[bfilpsv]", RAISED + UNRAISED + LETTER_SPACER)
	.replaceAll("[denoyz]", UNRAISED + RAISED + LETTER_SPACER)
	.replaceAll("[ghjqrtw]", RAISED + RAISED + LETTER_SPACER)
	.replaceAll("[ackmux]", UNRAISED + UNRAISED + LETTER_SPACER);

	return midText.trim();
	}

	
	public static String translateBottomLine(String text3) {

	String lowText = text3.trim().toLowerCase().replace(" ", WORD_SPACER);

	String textBot = lowText.replaceAll("[klmnopqrst]", RAISED + UNRAISED + LETTER_SPACER)
	.replaceAll("[w]", UNRAISED + RAISED + LETTER_SPACER)
	.replaceAll("[uvxyz]", RAISED + RAISED + LETTER_SPACER)
	.replaceAll("[abcdefghij]", UNRAISED + UNRAISED + LETTER_SPACER);

	return textBot.trim();
	}

	
	public static String translate(String finalText) {
	String a = translateTopLine(finalText);
	String b = translateMiddleLine(finalText);
	String c = translateBottomLine(finalText);

	String texts = finalText.format("%s%n%s%n%s%n", a, b, c);

	return texts;
	}
}

