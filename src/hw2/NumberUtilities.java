package hw2;

public class NumberUtilities {
	public static void main(String[] args) {
		
		System.out.println(round10(.6));
		System.out.println(getGameCount(3));
		System.out.println(getFraction(1.6));	
	}
	
	public static int round10(double firstNumber){
		
		float secondNumber = (float) firstNumber/10;
		int thirdNumber = (int) Math.round(secondNumber);
		int roundedNumber = thirdNumber * 10;
		
		return roundedNumber;	
	}
	
	public static int getGameCount(int roundCount){
		
		int gameCount = (int) Math.pow(2, roundCount) - 1;
		
		return gameCount;
	}
	
	public static double getFraction(double decimals){
		
		decimals = Math.abs(decimals);
		double newNumber = Math.floor(decimals);
		double fraction = decimals - newNumber;
		
		return fraction;			
	}

}
