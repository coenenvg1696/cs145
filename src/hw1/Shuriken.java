package hw1;

import java.util.Scanner;


public class Shuriken {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		System.out.print("Spin? ");
		double spin = in.nextDouble();
		
		double t = 0;
		double x = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.cos(Math.toRadians(t + spin));
		double y = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.sin(Math.toRadians(t + spin));
		System.out.printf("%.2f,%.2f%n",x,y );
		
		t = 30;
		x = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.cos(Math.toRadians(t + spin));
		y = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.sin(Math.toRadians(t + spin));
		System.out.printf("%.2f,%.2f%n",x,y );
		
		t = 60;
		x = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.cos(Math.toRadians(t + spin));
		y = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.sin(Math.toRadians(t + spin));
		System.out.printf("%.2f,%.2f%n",x,y );
		
		t = 90;
		x = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.cos(Math.toRadians(t + spin));
		y = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.sin(Math.toRadians(t + spin));
		System.out.printf("%.2f,%.2f%n",x,y );
		
		t = 120;
		x = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.cos(Math.toRadians(t + spin));
		y = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.sin(Math.toRadians(t + spin));
		System.out.printf("%.2f,%.2f%n",x,y );
		
		t = 150;
		x = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.cos(Math.toRadians(t + spin));
		y = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.sin(Math.toRadians(t + spin));
		System.out.printf("%.2f,%.2f%n",x,y );
		
		t = 180;
		x = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.cos(Math.toRadians(t + spin));
		y = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.sin(Math.toRadians(t + spin));
		System.out.printf("%.2f,%.2f%n",x,y );
		
		t = 210;
		x = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.cos(Math.toRadians(t + spin));
		y = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.sin(Math.toRadians(t + spin));
		System.out.printf("%.2f,%.2f%n",x,y );
		
		t = 240;
		x = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.cos(Math.toRadians(t + spin));
		y = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.sin(Math.toRadians(t + spin));
		System.out.printf("%.2f,%.2f%n",x,y );
		
		t = 270;
		x = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.cos(Math.toRadians(t + spin));
		y = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.sin(Math.toRadians(t + spin));
		System.out.printf("%.2f,%.2f%n",x,y );
		
		t = 300;
		x = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.cos(Math.toRadians(t + spin));
		y = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.sin(Math.toRadians(t + spin));
		System.out.printf("%.2f,%.2f%n",x,y );
		
		t = 330;
		x = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.cos(Math.toRadians(t + spin));
		y = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.sin(Math.toRadians(t + spin));
		System.out.printf("%.2f,%.2f%n",x,y );
		
		t = 360;
		x = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.cos(Math.toRadians(t + spin));
		y = (1.1 + Math.sin(Math.toRadians(4*t))) * Math.sin(Math.toRadians(t + spin));
		System.out.printf("%.2f,%.2f%n",x,y );
		
	}

}
