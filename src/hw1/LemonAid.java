package hw1;

import java.util.Scanner;

public class LemonAid {
	public static void main (String[] args){
		
		Scanner in = new Scanner(System.in);
		
		System.out.print("How many parts lemon juice? ");
		int nlemonJuice = in.nextInt();
		
		System.out.print("How many parts sugar? ");
		int nsugar = in.nextInt();

		System.out.print("How many parts water? ");
		int nwater = in.nextInt();

		System.out.print("How many cups of lemonade? ");
		int ncups = in.nextInt();

		
		int nparts = (nlemonJuice + nsugar + nwater);
				
		
		double lemonJuice = ((double) nlemonJuice * ncups / nparts);
		double sugar = ((double)nsugar * ncups / nparts);
		double water = ((double)nwater * ncups / nparts);

		System.out.println("Amounts (in cups):");
		System.out.println("  Lemon juice: " + lemonJuice);
		System.out.println("  Sugar: " + sugar);
		System.out.println("  Water: " + water);

	}
}
