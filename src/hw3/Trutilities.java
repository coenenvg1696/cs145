package hw3;

import java.io.File;
import java.util.GregorianCalendar;

public class Trutilities {
	public static boolean isOrdered(int a, int b, int c, int d, int e) {

		boolean inOrder = (a <= b && b <= c && c <= d && d <= e)
				|| (e <= d && d <= c && c <= b && b <= a);

		return inOrder;

	}

	public static boolean isGreenish(String hexcolor) {

		int redToInt = Integer.parseInt(hexcolor.substring(hexcolor.indexOf("#") + 1, 3), 16);
		int greenToInt = Integer.parseInt(hexcolor.substring(hexcolor.indexOf("#") + 3, 5), 16);
		int blueToInt = Integer.parseInt(hexcolor.substring(hexcolor.indexOf("#") + 5, 7), 16);

		boolean supaGreen = (greenToInt > redToInt && greenToInt > blueToInt);

		return supaGreen;
	}

	public static boolean isMilitary(String milTime) {

		int hours = Integer.parseInt(milTime.substring(0, 2));
		int minutes = Integer.parseInt(milTime.substring(2, 4));
		boolean realTime = (hours < 24 && minutes < 60);

		return realTime;
	}

	public static boolean isImage(File file) {

		String fileName = file.toString();
		fileName = fileName.toLowerCase();
		boolean goodEnding = fileName.endsWith(".png") || fileName.endsWith(".gif") || fileName.endsWith(".jpg")
				|| fileName.endsWith(".jpeg");

		return goodEnding;
	}

	public static boolean hasMultipleDots(String suchDots) {

		int lengthWithDots = suchDots.length();
		int lengthWithoutDots = suchDots.replace(".", "").length();
		int numOfDots = lengthWithDots - lengthWithoutDots;
		boolean containsMultipleDots = numOfDots >= 2;
		return containsMultipleDots;
	}

	public static boolean fitsAspect(int width, int height, double aspectRatio) {

		double exactAR = (double) width / (double) height;
		boolean doesFit = Math.abs(exactAR - aspectRatio) < .001;

		return doesFit;
	}

	public static boolean fitsWithin(int widthA, int heightA, int widthB, int heightB) {

		double areaRectA = (widthA * heightA);
		double areaRectB = (widthB * heightB);
		boolean rectDoesFit = (areaRectA <= areaRectB);

		return rectDoesFit;
	}

	public static boolean isFaster(String timeA, String timeB) {

		int hoursA = Integer.parseInt(timeA.substring(0, timeA.indexOf(":")));
		int minsA = Integer.parseInt(timeA.substring(timeA.indexOf(":") + 1, timeA.length()));
		int hoursB = Integer.parseInt(timeB.substring(0, timeB.indexOf(":")));
		int minsB = Integer.parseInt(timeB.substring(timeB.indexOf(":") + 1, timeB.length()));

		int timeInMinsA = (hoursA * 60) + minsA;
		int timeInMinsB = (hoursB * 60) + minsB;

		boolean suchFaster = timeInMinsA < timeInMinsB;

		return suchFaster;
	}

	public static boolean isIllegal(String direction) {

		String lowerDirection = direction.toLowerCase();
		boolean isLegal = !(lowerDirection.equals("north"))
				&& !(lowerDirection.equals("south"))
				&& !(lowerDirection.equals("east"))
				&& !(lowerDirection.equals("west"));

		return isLegal;
	}

	 public static boolean isOld(File fileThing, int year, int month, int day){
		 GregorianCalendar gCalendar = new GregorianCalendar(year, month - 1, day);
		 long timeInMilliseconds = fileThing.lastModified();
		 
		 return gCalendar.getTimeInMillis() >= timeInMilliseconds;
	 }
}