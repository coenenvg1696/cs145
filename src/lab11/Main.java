package lab11;


import splatbot.Splatter;

public class Main {
	
	public static void main(String[] args) {
		
		int delay = 1;
		int nTurns =500;
		
		new Splatter(splatbot.RobotLefty.class, lab11.SplatBot.class, delay, nTurns);
	}

}
