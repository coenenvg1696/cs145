package lab11;

import splatbot.Action;
import splatbot.Cell;
import splatbot.SplatBotColor;

public class SplatBot {
	
	public SplatBot(SplatBotColor color){
		
		color = SplatBotColor.BLUE;		
	}
	
	public Action getAction(Cell left, Cell center, Cell right){
		
		if (center == Cell.NEUTRAL || center == Cell.RED){
			return Action.MOVE_FORWARD;
		}
		if (left == Cell.NEUTRAL || left == Cell.RED){
			return Action.TURN_LEFT;
		}
		if (right == Cell.NEUTRAL || right == Cell.RED){
			return Action.TURN_RIGHT;
		}
		if ((center == Cell.ROCK || center == Cell.WALL) && (right == Cell.ROCK || right == Cell.WALL)){
			return Action.TURN_LEFT;
		}
		if ((center == Cell.ROCK || center == Cell.WALL) && (left == Cell.ROCK || left == Cell.WALL)){
			return Action.TURN_RIGHT;
		}
		if (center == Cell.ROCK || center == Cell.WALL){
			return Action.TURN_LEFT;
		}
		if (center == Cell.BLUE){
			return Action.MOVE_FORWARD;
		}
		if (center == Cell.RED_ROBOT){
			return Action.SPLAT;
		}
		
		
		return Action.SURVEY;
	}

	public void survey(Cell[][] cells){
		
	}
}
