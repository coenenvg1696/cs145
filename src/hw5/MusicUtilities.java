package hw5;

import hw5.speccheck.Note;

public class MusicUtilities {
	
	
	public static Note[] getScale(Note root, int[] halfsteps) {
		
		int length = halfsteps.length + 1;
		Note[] scale = new Note[length];
		
		for (int i = 0; i < scale.length; i++) {
			
			if (i == 0) {
				
				scale[i] = root;
			} else if (i == scale.length - 1) {
				
				scale[i] = new Note(scale[i - 1].getHalfstepID() + halfsteps[halfsteps.length - 1],
						root.getDuration(), root.getAmplitude(), root.isDotted());
			} else {
				
				scale[i] = new Note(scale[i - 1].getHalfstepID() + halfsteps[i - 1], root.getDuration(),
						root.getAmplitude(), root.isDotted());
			}
		}
		return scale;
	}
	

	public static Note[] getMajorScale(Note root) {
		
		Note[] majorScale = new Note[8];
		int x = root.getHalfstepID();

		for (int i = 0; i < majorScale.length; i++) {
			
			if (i == 0) {
				
				majorScale[i] = root;
			} else if (i == 3 || i == 7) {
				
				majorScale[i] = new Note(x + 1, root.getDuration(), root.getAmplitude(), root.isDotted());
				x += 1;
			} else {
				
				majorScale[i] = new Note(x + 2, root.getDuration(), root.getAmplitude(), root.isDotted());
				x += 2;
			}
		}
		return majorScale;
	}
	

	public static Note[] getMinorPentatonicBluesScale(Note root) {
		
		Note[] scale = new Note[6];
		int x = root.getHalfstepID();

		for (int i = 0; i < scale.length; i++) {
			
			if (i == 0) {
				
				scale[i] = root;
			} else if (i == 1 || i == 4) {
				
				scale[i] = new Note(x + 3, root.getDuration(), root.getAmplitude(), root.isDotted());
				x += 3;
			} else {
				
				scale[i] = new Note(x + 2, root.getDuration(), root.getAmplitude(), root.isDotted());
				x += 2;
			}
		}
		return scale;
	}
	

	public static Note[] getBluesScale(Note root) {
		
		Note[] bluesScale = new Note[7];
		int x = root.getHalfstepID();

		for (int i = 0; i < bluesScale.length; i++) {
			
			if (i == 0) {
				
				bluesScale[i] = root;
			} else if (i == 1 || i == 5) {
				
				bluesScale[i] = new Note(x + 3, root.getDuration(), root.getAmplitude(), root.isDotted());
				x += 3;
			} else if (i == 2 || i == 6) {
				
				bluesScale[i] = new Note(x + 2, root.getDuration(), root.getAmplitude(), root.isDotted());
				x += 2;
			
			} else {
				
				bluesScale[i] = new Note(x + 1, root.getDuration(), root.getAmplitude(), root.isDotted());
				x += 1;
			}
		}
		return bluesScale;
	}
	

	public static Note[] getNaturalMinorScale(Note root) {
		
		Note[] minorScale = new Note[8];
		int x = root.getHalfstepID();

		for (int i = 0; i < minorScale.length; i++) {
			
			if (i == 0) {
				minorScale[i] = root;
			} else if (i == 2 || i == 5) {
				
				minorScale[i] = new Note(x + 1, root.getDuration(), root.getAmplitude(), root.isDotted());
				x += 1;
			} else {
				
				minorScale[i] = new Note(x + 2, root.getDuration(), root.getAmplitude(), root.isDotted());
				x += 2;
			}
		}
		return minorScale;
	}

	
	public static Note[] join(Note[] firstPart, Note[] secondPart) {
		
		Note[] mixedParts = new Note[firstPart.length + secondPart.length];
		
		for (int i = 0; i < mixedParts.length; i++) {
			
			if (i < firstPart.length) {
				
				mixedParts[i] = firstPart[i];
			} else {
				
				mixedParts[i] = secondPart[i - firstPart.length];
			}
		}
		return mixedParts;
	}
	

	public static Note[] repeat(Note[] repeatedScale, int numOfRepeats) {
		
		Note[] song = new Note[repeatedScale.length * numOfRepeats];

		for (int i = 0; i < song.length; i++) {
			
			song[i] = repeatedScale[i % repeatedScale.length];
		}
		return song;
	}
	

	public static Note[] ramplify(Note[] baseScale, double startAmp, double endAmp) {
		
		Note[] ampedScale = new Note[baseScale.length];
		
		for (int i = 0; i < baseScale.length; i++) {
			
			if (i == 0) {
				
				ampedScale[i] = new Note(baseScale[i].getHalfstepID(), baseScale[i].getDuration(), startAmp, baseScale[i].isDotted());
			} else if (baseScale.length == 2) {
				
				ampedScale[0] = new Note(baseScale[0].getHalfstepID(), baseScale[0].getDuration(), startAmp, baseScale[0].isDotted());
				
				ampedScale[1] = new Note(baseScale[1].getHalfstepID(), baseScale[1].getDuration(), endAmp, baseScale[1].isDotted());

			} else {
				ampedScale[i] = new Note(baseScale[i].getHalfstepID(), baseScale[i].getDuration(), startAmp + (i * ((endAmp - startAmp) / ((double) baseScale.length - 1))), baseScale[i].isDotted());
			}
		}
		return ampedScale;
	}
	

	public static Note[] reverse(Note[] original) {
		
		Note[] reversed = new Note[original.length];

		for (int i = 0; i < reversed.length; i++) {
			
			reversed[i] = original[original.length - i - 1];
		}
		return reversed;
	}
	

	public static Note[] transpose(Note[] original, Note newRoot) {
		
		Note[] transposed = new Note[original.length];
		int difference = original[0].getHalfstepID() - newRoot.getHalfstepID();
		
		for (int i = 0; i < original.length; i++) {
			
			transposed[i] = new Note(original[i].getHalfstepID() - difference, original[i].getDuration(), original[i].getAmplitude(), original[i].isDotted());
		}
		return transposed;
	}
	

	public static Note[] invert(Note[] original, Note pivot) {
		
		Note[] pivoted = new Note[original.length];
		
		for (int i = 0; i < pivoted.length; i++) {
			
			if (original[i].getHalfstepID() > pivot.getHalfstepID()) {
				
				int distance = original[i].getHalfstepID() - pivot.getHalfstepID();
				pivoted[i] = new Note(pivot.getHalfstepID() - distance, original[i].getDuration(), original[i].getAmplitude(), original[i].isDotted());
					
			} else if (original[i].getHalfstepID() == pivot.getHalfstepID()) {
				
				pivoted[i] = new Note(pivot.getHalfstepID(), original[i].getDuration(), original[i].getAmplitude(), original[i].isDotted());
				
			} else if (original[i].getHalfstepID() < pivot.getHalfstepID()) {
				
				int distance = pivot.getHalfstepID() - original[i].getHalfstepID();
				pivoted[i] = new Note(pivot.getHalfstepID() + distance, original[i].getDuration(), original[i].getAmplitude(), original[i].isDotted());
			}

		}
		return pivoted;
	}

}
