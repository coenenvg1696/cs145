package hw7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class DondeUtilities {

	public static PlacesCache readCSV(File file) throws FileNotFoundException, IOException {
		
		PlacesCache locationName = new PlacesCache();
		String[] s = new String[file.toString().length()];
		String personName = "";
		String placeName = "";
		
		try {
			
			Scanner scanner = new Scanner(file).useDelimiter("\\Z");
			

			while (scanner.hasNext()) {
				s = scanner.nextLine().split("\\|", -1);
				personName = s[0];
				placeName = s[1];
				
				for (int i = -1; i < locationName.size(); ++i) {

					Place placeCreated = locationName.getPlace(placeName);
					placeCreated.addPerson(personName);
				}
			}
		} catch (NoSuchPlaceException exception) {

		}
		return locationName;

	}

	public static void writeKML(PlacesCache placesCache, File file) throws FileNotFoundException {
		PrintWriter outputFile = new PrintWriter(file);

		outputFile.print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + System.getProperty("line.separator") + "<kml xmlns=\"http://www.opengis.net/kml/2.2\">" + System.getProperty("line.separator") + "<Document>" + System.getProperty("line.separator") + "<name>Donde</name>" + System.getProperty("line.separator"));
		
		for (int i = 0; i < placesCache.size(); ++i) {
			
			outputFile.print((placesCache.get(i)).toKML() + System.getProperty("line.separator"));
		}
		
		outputFile.print("</Document>" + System.getProperty("line.separator") + "</kml>" + System.getProperty("line.separator"));
		outputFile.close();
	}

}