package hw7;

import java.io.IOException;
import java.util.ArrayList;


public class PlacesCache {
	private ArrayList<Place> locations;

	public PlacesCache(){
		locations = new ArrayList<Place>();
	}

	
	public boolean isCached(String location){
		String locationName = "";
		
		for(int i = 0; i < size(); ++i){
			locationName = locations.get(i).getName();	
			
			if (locationName.equals(location)){
				return true;
			}
		}
		return false;		
	}

	
	public Place getPlace(String locationName) throws IOException, NoSuchPlaceException {
		
		if (isCached(locationName)){
			for (int i = 0; i < locations.size(); ++i){
				if(locations.get(i).getName().equals(locationName)){
					return locations.get(i);
				}

			}
		}
		Place tempLocation = new Place(locationName);
		locations.add(tempLocation);
		return tempLocation; 
	}   

	
	public int size(){
		return locations.size();
	}

	public Place get(int i) throws IndexOutOfBoundsException{
		if(i>size()) {
			throw new IndexOutOfBoundsException();
		} else
			return locations.get(i);
	}
}