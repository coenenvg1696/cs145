package hw7;

import java.util.ArrayList;

public class Set {
	private ArrayList<String> string;

	public Set() {

		string = new ArrayList<>();
	}

	public boolean has(String innerString) {

		return string.contains(innerString);
	}

	public void add(String addedString) {
		
		if (has(addedString) != true){
			
			string.add(addedString);
		} 
	}

	public String toString() {
		
		String finalString = "";
		for (int i = 0; i < string.size(); ++i) {
			
			if (i == string.size() - 1) {
				
				finalString += string.get(i);
				
			} else {
				finalString += string.get(i) + System.getProperty("line.separator");
			}
		}
		return finalString;
	}

}
