package hw7;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;


public class Place {
	private String getName;
	private double getLongitude;
	private double getLatitude;
	private Set nameAdd = new Set();

	public String getName(){
		return getName;
	}

	public double getLongitude(){
		return getLongitude;
	}

	public double getLatitude(){
		return getLatitude;
	}

	public Place(String locationName, double latitude, double longitude){
		nameAdd = new Set();
		getName = locationName;
		getLongitude = longitude;
		getLatitude = latitude;
	}

	public URL toGeocodeURL() throws URISyntaxException, MalformedURLException {
		URI address = new URI("http", "www.twodee.org", "/teaching/cs145/2016c/homework/hw7/geocode.php", "place=" + this.getName, null);
		URL location = address.toURL();
		return location;
	}

	public Place(String locationName) throws IOException, NoSuchPlaceException {

		getName = locationName;
		try{
			URL url =  toGeocodeURL();
			Scanner coordnates = new Scanner(WebUtilities.slurpURL(url));
			while (coordnates.hasNext()){
				getLatitude = coordnates.nextDouble();
				getLongitude = coordnates.nextDouble();
			}
		} catch (Exception exception){
			throw new NoSuchPlaceException(locationName);
		}

	}

	public void addPerson(String personName){
		nameAdd.add(personName);
	}

	public String toKML(){
		
		String one = ("<Placemark>" + System.getProperty("line.separator"));
		String two = ("<name>" + getName() + "</name>" + System.getProperty("line.separator"));
		String three = ("<description>" + nameAdd.toString() + "</description>" + System.getProperty("line.separator"));
		String four = ("<Point><coordinates>" + String.format("%.2f", getLongitude()) + "," + String.format("%.2f", getLatitude()) + "</coordinates></Point>" + System.getProperty("line.separator"));
		String five = ("</Placemark>");
		
		String kml = (one + two + three + four + five);
		return kml;
	}
}