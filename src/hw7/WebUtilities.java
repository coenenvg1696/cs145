package hw7;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class WebUtilities {

	public static String slurpURL(URL url) throws IOException {
		
		URLConnection connection = url.openConnection();
		InputStream is = connection.getInputStream();
		Scanner scanner = new Scanner(is);
		String adress = "";
		while (scanner.hasNext()) {
			adress += scanner.nextLine();
		}
		return adress;
	}

}